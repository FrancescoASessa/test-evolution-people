<?php
Flight::register('db', 'medoo', array(
	array(
		'database_type' => DB_TYPE,
		'database_name' => DB_NAME,
		'server' => DB_HOST,
		'username' => DB_USER,
		'password' => DB_PASS,
		'charset' => DB_CHARSET,
		'port' => DB_PORT,
		'prefix' => DB_PREFIX,
		'option'        => array(
			\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
		)
	)
));	

?>