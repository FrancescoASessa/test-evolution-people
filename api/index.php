<?php
require 'core/flight/Flight.php';
require 'thirdparty/Medoo/medoo.php';
require 'config/settings.php';
require 'config/database.php';
require 'register.php';
require 'class/user.php';
require 'route.php';

Flight::start(); // Starts Flight framework.
?>
