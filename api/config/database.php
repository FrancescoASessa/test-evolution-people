<?php
//Database Config
define("DB_TYPE", "mysql");
define("DB_HOST", "localhost");
define("DB_PORT", 3306);
//Database Credentials
define("DB_USER", "");
define("DB_PASS", "");
//Table name
define("DB_NAME", "");
//Table prefix
define("DB_PREFIX", "");
//Database Charset
define("DB_CHARSET", "utf8");
?>