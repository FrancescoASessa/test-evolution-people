<?php
//The following is a list of all the available configuration settings:

Flight::set('flight.base_url', null); //Override the base url of the request. (default: null)
Flight::set('flight.case_sensitive', false); //Case sensitive matching for URLs. (default: false)
Flight::set('flight.handle_errors', true); //Allow Flight to handle all errors internally. (default: true)
Flight::set('flight.log_errors', false); //Log errors to the web server's error log file. (default: false)
//For more... (http://flightphp.com/learn/#configuration)
?>