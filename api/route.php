<?php
	Flight::route('GET /user/@user_id:[0-9]+/hobbies',   array('User','getHobbies')); 
	Flight::route('GET /user/@user_id:[0-9]+/info',   array('User','getInfo')); 
	Flight::route('POST /add/user',   array('User','register')); 
	
	Flight::route('GET /list/hobbies',   array('User','getAllHobbies')); 
	Flight::route('GET /list/users',   array('User','getAllUsers')); 
?>