<?php
class User {
	public function getAllUsers() {
		$db = Flight::db();
		try{
			$hobbies = $db->pdo->prepare("
			SELECT user_data.nome,
			user_data.cognome,
			user_data.gender,
			users.email,
			users.password 
			FROM user_data 
			INNER JOIN users 
			ON users.id = user_data.user_id 
			WHERE users.status = 1
			");
			$hobbies->execute();
			return Flight::json(["status" => true,"content" => $hobbies->fetchAll(PDO::FETCH_ASSOC)]);
		}catch(PDOException $e) {
			return	Flight::json(["status" => false,"msg" => $e->errorInfo[2],"error_code" => $e->errorInfo[1]]);
		}		
	}
	
	public function getAllHobbies() {
		$db = Flight::db();
		try{
			$hobbies = $db->pdo->prepare("
			SELECT DISTINCT hobbies.id,hobbies.name 
			FROM hobbies
			");
			$hobbies->execute();
			return Flight::json(["status" => true,"content" => $hobbies->fetchAll(PDO::FETCH_ASSOC)]);
		}catch(PDOException $e) {
			return	Flight::json(["status" => false,"msg" => $e->errorInfo[2],"error_code" => $e->errorInfo[1]]);
		}		
	}
	
	public function getHobbies($user_id) {
		$db = Flight::db();
		try{
			$hobbies = $db->pdo->prepare("
			SELECT DISTINCT hobbies.name 
			FROM hobbies 
			INNER JOIN user_hobbies 
			ON user_hobbies.hobbies_id = hobbies.id 
			WHERE user_hobbies.user_id = :user_id
			");
			$hobbies->bindParam(':user_id',$user_id,PDO::PARAM_INT);
			$hobbies->execute();
			return Flight::json(["status" => true,"content" => $hobbies->fetchAll(PDO::FETCH_ASSOC)]);
		}catch(PDOException $e) {
			return	Flight::json(["status" => false,"msg" => $e->errorInfo[2],"error_code" => $e->errorInfo[1]]);
		}
	}
	
	public function getInfo($user_id) {
		$db = Flight::db();
		try{
			$hobbies = $db->pdo->prepare("
			SELECT user_data.nome,
			user_data.cognome,
			user_data.gender,
			users.email,
			users.password 
			FROM user_data 
			INNER JOIN users 
			ON users.id = user_data.user_id 
			WHERE users.id = :user_id AND users.status = 1
			");
			$hobbies->bindParam(':user_id',$user_id,PDO::PARAM_INT);
			$hobbies->execute();
			return Flight::json(["status" => true,"content" => $hobbies->fetchAll(PDO::FETCH_ASSOC)]);
		}catch(PDOException $e) {
			return	Flight::json(["status" => false,"msg" => $e->errorInfo[2],"error_code" => $e->errorInfo[1]]);
		}
	}
	
	public function register() {

		$hobbies = explode(",",Flight::request()->data->hobbies); 

		$db = Flight::db();
		
		if (filter_var(Flight::request()->data->email, FILTER_VALIDATE_EMAIL)) {
			if(Flight::request()->data->password == Flight::request()->data->Rpassword)
				{
					try {
						$user_id = $db->insert("users", [
							"email" => Flight::request()->data->email,
							"password" => password_hash(Flight::request()->data->password, PASSWORD_BCRYPT),
							"date" => date("Y-m-d H:i:s"),
							"status" => "1"
						]);
						$user_data_id = $db->insert("user_data", [
							"user_id" => $user_id,
							"nome" => Flight::request()->data->nome,
							"cognome" => Flight::request()->data->cognome,
							"gender" => Flight::request()->data->gender
						]);
						for($i = 0; $i < count($hobbies); $i++){
							$hobby_saved_id = $db->insert("user_hobbies", [
								
									"user_id" => $user_id,
									"hobbies_id" => $hobbies[$i]
								
							]);						
						}
						return Flight::json(["status" => true,"msg" => "OK"]);
					} catch (PDOException $e) {
						Flight::halt(409, $e->errorInfo[2]);
						
				}
				}
		} 
	}
	
}
?>